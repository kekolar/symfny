https://www.ambionics.io/blog/symfony-secret-fragment

Install
```
pip3 install -r requirements.txt
```
Usage
```
cat urls.txt | python3 symfony_checker.py --stdin --thread 100

cat urls.txt | python3 symfony_checker.py --stdin --thread 100 --key blablablablablablabla

cat urls.txt | python3 symfony_checker.py --stdin --thread 100 --key wordlist.txt --output save_results.txt

python3 symfony_checker.py -u https://example.com --thread 1
```

symfony_wordlist_parser.py brute force yöntemi yapmak için kullanılabilir.
rockyou.txt ile birlikte kullanılan bir yardımcı programdır. Eğer programın klasörüne daha önceden rockyou.txt indirmediyseniz program bunu kendisi yapacaktır.
```
python3 symfony_wordlist_parser.py https://target.com
```
